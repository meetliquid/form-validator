# README #

### Simple Validate ###

Simple Validate es un pequeño y flexible plugin para validar campos dentro de un contenedor padre.

- Devuelve TRUE si todos los campos cumplen con la validación, de lo contrario devuelve FALSE.
- Agrega la clase "error" al padre del campo inválido.

### Resume ###

* Plugin Simple Validate
* Version: 1.0
* Created by: José Ramos
* Date: 01/05/2014

### Modo de uso ###

Si usted desea validar un campo agregar el atributo "data-valid" con uno de los siguientes valores:

- required: Cuando el campo sea obligatorio.
- integer: Cuando el campo sea de números enteros.
- email: Cuando el campo sea un correo electrónico.
- dni: Cuando el campo sea un DNI.

Ejemplo:
```html
<form id="frdata">
	<div><h2>Nombre</h2><input type="text"  data-valid="required" /></div>
	<div><h2>DNI</h2><input type="text"  data-valid="dni" maxlength="8" /></div>
	<div><h2>País</h2>
		<select data-valid="required">
			<option value="">Seleccione</option>
			<option value="1">Alemania</option>
			<option value="2">Brasil</option>
			<option value="3">Perú</option>
		</select>
	</div>
	<div><h2>Correo</h2><input type="text" data-valid="email" /></div>
	<div><h2>Términos</h2><input type="checkbox" data-valid="required" /> Acepta los términos y condiciones</div>
	
	<button id="btn">Enviar</button>
</form>
```

Agregamos una acción al evento del botón "enviar"
```js
$('#frdata').blurValidate();

$('#btn').click(function(e){
	e.preventDefault();
	var valid = $('#frdata').formValidate();//frdata es el id del contenedor padre
	if(valid){
		// formulario correcto
	}
});
```