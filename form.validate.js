$.fn.formValidate = function(options) {
	var defaults = { 'errorclass':'error' }
	var opts = $.extend(defaults, options);
	var bool = true, finish = true;	
	var inputs = this.find('*[data-valid]');
	
	inputs.each(function(){
		var myelem = $(this);
		var parent = myelem.parent();
		
		switch(myelem.attr('data-valid')){
			case 'email': 		bool = validEmail(myelem.val()); break;
			case 'required':	bool = validRequired(myelem);  break;
			case 'integer': 	bool = validInt(myelem.val());  break;
			case 'dni': 		bool = validDni(myelem.val()); break;
		}
			
		if(!bool || myelem.val() == myelem.attr('data-holder')){
			parent.addClass(opts.errorclass);
			finish = false;
			bool = true;			
		}else{
			parent.removeClass(opts.errorclass);
		}
	});	
	
	function validEmail(valor){ return (/^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(valor)) ? true : false; }
	function validInt(valor){ return (/^\d+$/.test(valor)) ? true : false; }
	function validDni(valor){ return (/^\d+$/.test(valor) && valor.length == 8) ? true : false; }
	function validRequired(elem){ 
		if(elem.is('select')){ return ($.trim(elem.val()) != '') ? true : false;
		}else if(elem.is('input[type=checkbox]')){ return (elem.prop('checked')) ? true : false;
		}else{ return (/[^.*\s]/.test(elem.val())) ? true : false; }
	}
		
	return finish;
};

$.fn.blurValidate = function(options) {
	var defaults = { 'errorclass':'error' }
	var opts = $.extend(defaults, options);
	var inputs = this.find('*[data-valid]');
	
	inputs.each(function(){
		var elem   = $(this);
		var parent = elem.parent();
		
		if(elem.is('input')){			
			elem.blur(function() { 
				parent.formValidate();
			});
		}
		
		if(elem.is('select')){
			elem.on("change blur",function() {
				parent.formValidate();
			});
		}
	});
}